<?php
    # This part is where we include all the class files

    # Framework
    require_once '../framework/Controller/ControllerTrait.php';
    require_once '../framework/Controller/BaseController.php';
    require_once '../framework/Database/Database.php';
    require_once '../framework/Service/RepositoryInterface.php';

    # Repository
    require_once '../src/Repository/TaskRepository.php';

    # Service
    require_once '../src/Service/TaskService.php';

    # Entities
    require_once '../src/Entity/Task.php';

    # Controllers
    require_once '../src/Controller/TaskController.php';