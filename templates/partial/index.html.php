<h1>To-Do</h1>
<div class="row">
    <div class="col-12">
        <ul class="list-unstyled">
            <li>Total Task: <strong><?=sizeof($tasks)?></strong></li>
            <li>Completed Task: <strong><?=$completedTaskCount?></strong></li>
        </ul>
        <a href="<?= $__baseUrl . '/public/task/new'?>" class="btn btn-primary">
            <i class="fa fa-plus"></i> Add a task
        </a>
    </div>
    <div class="col-md-4 p-3">
        <div class="w-100 bg-light border p-3">
            <h5 class="text-center">To-Do</h5>
            <?php foreach ($tasks as $task): ?>
                <?php if ($task->getStatus() === $task::TASK_STATUS_TODO): ?>
                    <a href="<?=$__baseUrl . '/public/task/' . $task->getId();?>" class="text-decoration-none text-black">
                        <div class="w-100 bg-white border p-3 my-1">
                            <?php if ($task->getPriority()): ?>
                                <i class="fa fa-star text-warning"></i>
                            <?php endif; ?>
                            <strong><?=$task->getName()?></strong>
                            <br>
                            <small><?=$task->getStatus()?></small>
                        </div>
                    </a>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="col-md-4 p-3">
        <div class="w-100 bg-light border p-3">
            <h5 class="text-center">On Going</h5>
            <?php foreach ($tasks as $task): ?>
                <?php if ($task->getStatus() === $task::TASK_STATUS_ONGOING): ?>
                    <a href="<?=$__baseUrl . '/public/task/' . $task->getId();?>" class="text-decoration-none text-black">
                        <div class="w-100 bg-white border p-3 my-1">
                            <?php if ($task->getPriority()): ?>
                                <i class="fa fa-star text-warning"></i>
                            <?php endif; ?>
                            <strong><?=$task->getName()?></strong>
                            <br>
                            <small><?=$task->getStatus()?></small>
                        </div>
                    </a>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="col-md-4 p-3">
        <div class="w-100 bg-light border p-3">
            <h5 class="text-center">Done</h5>
            <?php foreach ($tasks as $task): ?>
                <?php if ($task->getStatus() === $task::TASK_STATUS_COMPLETED): ?>
                    <a href="<?=$__baseUrl . '/public/task/' . $task->getId();?>" class="text-decoration-none text-black">
                        <div class="w-100 bg-white border p-3 my-1">
                            <?php if ($task->getPriority()): ?>
                                <i class="fa fa-star text-warning"></i>
                            <?php endif; ?>
                            <strong><?=$task->getName()?></strong>
                            <br>
                            <small><?=$task->getStatus()?></small>
                        </div>
                    </a>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>