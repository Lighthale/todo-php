<div class="row">
    <div class="col-md-4 offset-md-4 border p-0">
        <h5 class="w-100 bg-primary text-white p-2">Create Task</h5>
        <div class="w-100 px-2 pb-2">
            <?php include_once 'form/task-form.html.php';?>
            <hr>
            <a href="<?=$__baseUrl . '/public';?>" class="btn btn-primary">
                <i class="fa fa-arrow-left"></i> Back to board
            </a>
            <a href="<?=$__baseUrl . '/public/task/' . $task->getId() . 'delete';?>" class="btn btn-danger">
                <i class="fa fa-trash"></i> Delete
            </a>
        </div>
    </div>
</div>