<form name="taskForm" method="post">
    <div class="input-group">
        <label for="taskName" class="form-label">Name:
            <input type="text" name="taskName" id="taskName" class="form-control" value="<?=$task->getName();?>" required/>
        </label>
    </div>
    <div class="input-group">
        <label for="taskStatus" class="form-label">Status:
            <select name="taskStatus" id="taskStatus" class="form-control form-select">
                <option value="<?=$task::TASK_STATUS_TODO;?>" <?=$task->getStatus() == $task::TASK_STATUS_TODO ? 'selected' : null;?>>To Do</option>
                <option value="<?=$task::TASK_STATUS_ONGOING;?>" <?=$task->getStatus() == $task::TASK_STATUS_ONGOING ? 'selected' : null;?>>Ongoing</option>
                <option value="<?=$task::TASK_STATUS_COMPLETED;?>" <?=$task->getStatus() == $task::TASK_STATUS_COMPLETED ? 'selected' : null;?>>Completed</option>
            </select>
        </label>
    </div>
    <div class="input-group">
        <label for="taskPriority" class="form-label">Priority:
            <input type="checkbox" name="taskPriority" id="taskPriority" class="form-check-inline" value="1" <?=$task->getPriority() ? 'checked' : null;?> />
        </label>
    </div>
    <input type="submit" class="btn btn-primary mb-2" value="Submit" />
</form>