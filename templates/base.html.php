<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>To-Do</title>

        <!-- CSS Plugins -->
        <link href="<?=$__baseUrl . '/public/vendor/bootstrap-5.1.3/css/bootstrap.min.css';?>" rel="stylesheet" type="text/css">
        <link href="<?=$__baseUrl . '/public/vendor/fontawesome/css/fontawesome.min.css';?>" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <?php include_once $__component;?>
        </div>
        <!-- Javascript Plugins -->
        <script src="<?=$__baseUrl . '/public/vendor/jquery/jquery.min.js';?>"></script>
        <script src="<?=$__baseUrl . '/public/vendor/bootstrap-5.1.3/js/bootstrap.min.js';?>"></script>
        <script src="<?=$__baseUrl . '/public/vendor/fontawesome/js/all.min.js';?>"></script>
    </body>
</html>