<?php

namespace App\Repository;

use App\Entity\Task;
use Framework\Database\Database;
use Framework\Service\RepositoryInterface;

/**
 * This is where queries on task table are stored.
 */
class TaskRepository implements RepositoryInterface
{
    private $database;

    /**
     * Constructor with Dependency Injection
     * Reference: https://www.getopensocial.com/blog/open-source/dependency-injection-php/
     *
     * @param Database $database
     */
    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    /**
     * Get all task records
     *
     * @param array $orderBy
     * @param int|null $limit
     * @return array
     */
    public function findAll(array $orderBy = [], int $limit = null): array
    {
        $query = 'SELECT * FROM task';

        # Adds order by statement in query string
        if ($orderBy) {
            $query .= ' ORDER BY';

            foreach ($orderBy as $key => $value) {
                $query .= ' ' . $key . ' ' . $value . ',';
            }

            # Removes trailing ","
            $query = rtrim($query, ',');
        }

        # Adds order by statement in query string
        if ($limit) {
            $query .= ' LIMIT ' . $limit;
        }

        $results = $this->database->query($query);
        $tasks = [];

        while ($row = $results->fetchArray()) {
            $task = new Task();
            $task->setId($row['id']);
            $task->setName($row['name']);
            $task->setStatus($row['status']);
            $task->setPriority($row['priority']);
            $tasks[] = $task;
        }

        return $tasks;
    }

    /**
     * Get one task by field
     *
     * @param array $field
     * @return Task
     * @throws \ErrorException
     */
    public function findOneBy(array $field = []): Task
    {
        if (!$field)
            throw new \ErrorException('field in findBy is required');

        $query = 'SELECT * FROM task WHERE';

        foreach ($field as $key => $value) {
            $query .= ' ' . $key . ' = ' . $value . ' AND ';
        }

        $query = rtrim($query, ' AND ');
        $query .= ' LIMIT 1';
        $results = $this->database->query($query);
        $row = $results->fetchArray();
        $task = new Task();
        $task->setId($row['id']);
        $task->setName($row['name']);
        $task->setStatus($row['status']);
        $task->setPriority($row['priority']);

        return $task;
    }

    /**
     * Create a task
     *
     * @param Task|null $task
     */
    public function create(Task $task = null): void
    {
        $query = "INSERT INTO task (name, status, priority) VALUES ('" . $task->getName() . "', '" . $task->getStatus() . "', '" . $task->getPriority() . "')";
        $this->database->exec($query);
    }

    /**
     * Update a task
     *
     * @param Task|null $task
     */
    public function update(Task $task = null): void
    {
        $query = "UPDATE task SET name = '" . $task->getName() . "', status = '" . $task->getStatus() . "', priority = " . $task->getPriority() . " WHERE id = " . $task->getId();
        $this->database->exec($query);
    }

    /**
     * Delete a task
     *
     * @param Task|null $task
     */
    public function delete(Task $task = null): void
    {
        $query = "DELETE FROM task WHERE id = '" . $task->getId() . "'";
        $this->database->query($query);
    }
}