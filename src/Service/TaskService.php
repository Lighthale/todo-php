<?php

namespace App\Service;

/**
 * This is where calculations are stored
 */
class TaskService
{
    /**
     * Counts completed status in the list of tasks
     *
     * @param array $tasks
     * @return int
     */
    public function countCompletedTask(array $tasks): int
    {
        $count = 0;

        foreach ($tasks as $task) {
            if ($task->getStatus() === $task::TASK_STATUS_COMPLETED)
                $count++;
        }

        return $count;
    }
}