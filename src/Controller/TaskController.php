<?php

namespace App\Controller;

use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Service\TaskService;
use Framework\Controller\BaseController;
use Framework\Database\Database;

class TaskController extends BaseController
{
    /**
     * route: '/public'
     */
    public function index()
    {
        $database = new Database();
        $taskRepository = new TaskRepository($database);
        $tasks = $taskRepository->findAll(['priority' => 'DESC', 'name' => 'ASC']);
        $taskService = new TaskService();
        $completedTaskCount = $taskService->countCompletedTask($tasks);

        return parent::render('partial/index.html.php', [
            'tasks' => $tasks,
            'completedTaskCount' => $completedTaskCount,
        ]);
    }

    /**
     * route: '/public/task/new'
     */
    public function new()
    {
        $task = new Task();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $database = new Database();
            $taskRepository = new TaskRepository($database);
            $task->setName($_POST['taskName']);
            $task->setStatus($_POST['taskStatus']);
            $task->setPriority($_POST['taskPriority'] ? 1 : 0);
            $taskRepository->create($task);

            header('Location: ' . $__baseUrl . '/public');
        }

        return parent::render('partial/new.html.php', [
            'task' => $task,
        ]);
    }

    /**
     * route: '/public/task/{id}'
     */
    public function edit(string $taskId)
    {
        $database = new Database();
        $taskRepository = new TaskRepository($database);
        $task = $taskRepository->findOneBy(['id' => $taskId]);

        if (!$task)
            throw new \ErrorException('Task not found.');

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $task->setName($_POST['taskName']);
            $task->setStatus($_POST['taskStatus']);
            $task->setPriority($_POST['taskPriority'] ? 1 : 0);
            $taskRepository->update($task);

            header('Location: ' . $__baseUrl . '/public');
        }

        return parent::render('partial/show.html.php', [
            'task' => $task,
        ]);
    }

    /**
     * route: '/public/task/{id}/delete'
     */
    public function delete(string $taskId)
    {
        $database = new Database();
        $taskRepository = new TaskRepository($database);
        $task = $taskRepository->findOneBy(['id' => $taskId]);

        if (!$task)
            throw new \ErrorException('Task not found.');

        $taskRepository->delete($task);

        header('Location: ' . $__baseUrl . '/public');
    }
}
