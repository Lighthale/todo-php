<?php

namespace App\Entity;

/**
 * Data from task table will be set to this entity/model
 */
class Task
{
    # Constant variables for task statuses
    const TASK_STATUS_TODO = 'todo';
    const TASK_STATUS_ONGOING = 'ongoing';
    const TASK_STATUS_COMPLETED = 'completed';

    # Task's private variables. These can be manipulated by using its getters and setters
    private $id;
    private $name;
    private $status;
    private $priority;

    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority): void
    {
        $this->priority = $priority;
    }
}
