## TODO PHP

### Requirements
- PHP 7.4
- SQLite3 (enable `extension=sqlite3` in php.ini)

### Localhost Setup
1. Open your terminal and go to the project's base path
2. Enter `php -S localhost:8080` to launch the app
3. Open your browser and go to `http://localhost:8080/public`