<?php

namespace Framework\Database;

use SQLite3;

/**
 * This class will be used to connect to the SQLite
 *
 * Make sure that "extension=sqlite3" is enabled in your php.ini file
 */
class Database extends SQLite3
{
    public function __construct() {
        $this->open('../database/todo.db');
    }
}